FROM rustlang/rust:nightly-slim

WORKDIR /usr/src/sheet-server
COPY . .

RUN cargo install --path .

CMD ["sheet-server"]

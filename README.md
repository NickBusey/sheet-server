sheet-server is a simple project to provide an API into spreadsheets.

# Usage

`cargo run` to start.

Hit http://localhost:8000/cell/<file_name>/<sheet_name>/<target_col>/<target_row> to return it's value

<target_col> is a 0 offset number rather than the letter of the column, e.g., column E should be passed as 4.

## Nightly Rust

Rocket requires the nightly version of rust.

Run `rustup toolchain install nightly`

Then `rustup default nightly`

To enable it.

# Docker

Run the following command inside a folder containing spreadsheets.

`docker run -d --name sheet-server -p 8000:8000 -v $(pwd)/:/usr/src/sheet-server nickbusey/sheet-server`

# Sample Spreadsheet

http://localhost:8000/cell/Sample.ods/Sheet1/1/1 should return '280', the value of the B2 cell of Sheet1 from Sample.ods.

# TODO:

* Alert based on upcoming dates.
* Add exploratory URLs (List files, list sheets in a file, list rows in a sheet, list columns in a row)
* Add writing/saving of values

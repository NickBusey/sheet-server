#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate simple_excel_writer as excel;

use calamine::{open_workbook, Ods, Reader};

#[get("/cell/<file_name>/<sheet_name>/<target_col>/<target_row>")]
fn cell(file_name: String, sheet_name: String, target_col: usize, target_row: usize) -> String {
    let mut values: String = "".to_owned();
    let mut excel: Ods<_> = open_workbook(file_name).unwrap();
    if let Some(Ok(r)) = excel.worksheet_range(&sheet_name.to_string()) {
        let mut count = 0;
        for row in r.rows() {
            if count == target_row {
                values.push_str(&String::from(format!("{}", row[target_col])));
            }
            count = count + 1;
        }
        return values;
    } else {
        return "Fail".to_owned();
    }
}

fn main() {
    rocket::ignite().mount("/", routes![cell]).launch();
}
